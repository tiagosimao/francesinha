## Sauce Ingredients (for 2 people)
* Olive oil
* 2 onions
* 3 garlic cloves
* 3 bay leaves
* 50g of bacon
* 4 table spoons of tomato sauce
* 0.2l of white wine
* 0.5l of beer
* 0.2l of meat stock
* 2 table spoons of seafood soup powder
* 0.5dl of Port Wine
* 0.5dl of Águardente velha (brandy)

## Mise en place
Chop the onions, garlic and bacon into small bits

## Cooking
Fill the bottom of a pan with olive oil and heat it  
Add the bay leaves, chopped onions, garlic and bacon to the pan. If the oil is hot, the onions should instantly start boiling  
Once the onions are golden, add the tomato sauce, mix everything. Wait until the mixture is boiling again  
Add the white wine and wait again for boiling  
Add the beer and leave it there for about 15 minutes, cooking along. Keep the lid out.  
In a separate small pan, dissolve the seafood powder in the meat stock  
After the 15 minutes have passed, add the stock to the main mixture and let re-boil  
Remove the bay leaves from the pan and grind the mixture with a blender until it's a homogeneous sauce  
Add the Port and Aguardente to the sauce and let it boil until you reached the target sauce density  

